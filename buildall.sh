#!/bin/sh
for i in `cat targets.txt`; do
  arch=$(echo $i | cut -d "_" -f1)
  subarch=$(echo $i | cut -d "_" -f2)
  ./build.sh $arch $subarch 23.05.0-rc2
done
